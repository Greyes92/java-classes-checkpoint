package com.galvanize;
import java.util.ArrayList;

public class CellPhone {
    private ArrayList<String> callHistory = new ArrayList<>();
    private CallingCard currentCallingCard;
    private String currentNumber;
    private int callLength = 0;
    private boolean isTalking = false;
    private boolean callCutOff = false;

    public CellPhone(CallingCard callingCard){
        currentCallingCard = callingCard;
    }

    public void call(String phoneNumber){
        isTalking = true;
        currentNumber = phoneNumber;
    }

    public void endCall(){
        setCallHistory();
        isTalking = false;
        currentNumber = "Please dial a number";
        callLength = 0;

    }
    public ArrayList<String> getCallHistory(){
        System.out.println(callHistory);
        return callHistory;
    }

    public void setCallHistory(){
        String minute = callLength > 1 ? "minutes" : "minute";

        if (callCutOff) {
            callHistory.add(String.format("%s (cut off at %s %s)", currentNumber, callLength, minute));
        } else {
            callHistory.add(String.format("%s (%s %s)", currentNumber, callLength, minute));
        }
    }

    public void tick(){
        callLength++;
        currentCallingCard.useMinutes(1);

        if(currentCallingCard.getRemainingMinutes() <= 0){
            callCutOff = true;
            endCall();
        }

    }




}
