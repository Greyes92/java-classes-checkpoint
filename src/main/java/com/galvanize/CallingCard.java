package com.galvanize;

public class CallingCard {
    private int rate;
    private int minutes;

    public CallingCard(int rate){
        this.rate = rate;
        this.minutes = 0;
    }

    public void addDollars(float dollars) {
        float cents = dollars * 100;
        minutes += cents/rate;
    }

    public int getRemainingMinutes() {
        System.out.println(minutes);
        return minutes;
    }

    public void useMinutes(int minute){
        minutes = minutes - minute;
    }
}
